﻿
using UnityEngine;

public class Player : MonoBehaviour
{
    private Vector3 direction;

    public float strength = 5f;

    public float gravity = -9.8f;

    private SpriteRenderer spriteRenderer;

    public Sprite[] sprites;

    private int spriteIndex;

    private void Start()
    {
        InvokeRepeating(nameof(AnimateSprite), 0.15f, 0.15f);
    }

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

    }

    private void OnEnable()
    {
        Vector3 position = transform.position;
        position.y = 0f;
        transform.position = position;
        direction = Vector3.zero;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            direction = Vector3.up * strength;
        }

        if(Input.touchCount>0)
        {
            Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                direction = Vector3.up * strength;
            }
              
                    
        }
        direction.y += gravity * Time.deltaTime;
        transform.position += direction * Time.deltaTime;

        /*i += 1;
        i = i + 1;*/
      

    }
    private void AnimateSprite()
    {
        spriteIndex++;
        if (spriteIndex >= sprites.Length)
        {
            spriteIndex = 0;
        }
        spriteRenderer.sprite = sprites[spriteIndex];
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "obstacle")
        {
            FindObjectOfType<GameManager>().GameOver();

        } else if (other.gameObject.tag == "scoring")
        {
            FindObjectOfType<GameManager>().IncreaseScore();
        }
    }


}
