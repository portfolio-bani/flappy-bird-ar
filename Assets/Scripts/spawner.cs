﻿
using UnityEngine;

public class spawner : MonoBehaviour
{
    public GameObject prefab;

    public float spawnRate = 1f ;

    public float minHeight = -1f;

    public float maxHeight = 1f;

    public void OnEnable()
    {
        InvokeRepeating(nameof(Spawn), spawnRate, spawnRate);

    }
    public void OnDisable()
    {
        CancelInvoke(nameof(Spawn));
    }
    public void Spawn()
    {
        GameObject Pipes = Instantiate( prefab, transform.position, Quaternion.identity);
        Pipes.transform.position += Vector3.up * Random.Range(minHeight, maxHeight);
    }

}
