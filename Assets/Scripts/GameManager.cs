﻿using UnityEngine.UI;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Player player;

    public Text scoretext;

    public GameObject playButton;

    public GameObject gameOver;



    private int Score;

    private void Awake()
    {
        Application.targetFrameRate = 60;
        Pause();
    }

    public void Play()
    {
        Score = 0;
        scoretext.text = Score.ToString();

        gameOver.SetActive(false);
        playButton.SetActive(false);

        player.enabled = true;
        Time.timeScale = 1f;

        pipeSpeed[] pipes = FindObjectsOfType<pipeSpeed>();
        for (int i=0; i<pipes.Length; i++)
        {
            Destroy(pipes[i].gameObject);
        }
    }

    public void Pause()
    {
        Time.timeScale = 0f;
        player.enabled = false;
    }

    public void GameOver()
    {
        gameOver.SetActive(true);
        playButton.SetActive(true);
        Pause();
    }

    public void IncreaseScore()
    {
        Score++;
        scoretext.text = Score.ToString();
    }
}
